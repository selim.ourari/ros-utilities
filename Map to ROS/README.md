# Map Conversion Program

This program allows you to convert a floor plan image to a ROS map using OpenCV. It prompts you for various input parameters and saves the resulting map in the desired location.

## Prerequisites

- Python 3.x
- OpenCV library (`pip install opencv-python`)
- NumPy library (`pip install numpy`)

## Usage

1. Clone the repository:

   ```shell
   git clone <repository_url>
   ```

2. Navigate to the repository directory:

   ```shell
   cd <repository_name>
   ```

3. Place the floor plan image in the same directory as the code snippet.

4. Run the code snippet using Python:

   ```shell
   python map_conversion.py
   ```

5. Follow the prompts in the command line to select points and provide necessary information for map conversion.

6. The resulting map (in PGM format) and its corresponding YAML file will be saved in the specified location.

## Code Snippet

```python
import cv2
import math
import os.path
import numpy as np

prompt = '> '

print("What is the name of your floor plan you want to convert to a ROS map:")
file_name = input(prompt)
print("You will need to choose the x coordinates horizontally with respect to each other")
print("Double click the first x point to scale")

# Read in the image
image = cv2.imread(file_name)

ix, iy = -1, -1
x1 = [0, 0, 0, 0]
y1 = [0, 0, 0, 0]
font = cv2.FONT_HERSHEY_SIMPLEX

def draw_point(event, x, y, flags, param):
    global ix, iy, x1, y1, sx, sy
    if event == cv2.EVENT_LBUTTONDBLCLK:
        ix, iy = x, y
        print(ix, iy)

        image[iy, ix] = (0, 0, 255)
        cv2.line(image, (ix + 2, iy), (ix + 10, iy), (0, 0, 255), 1)
        cv2.line(image, (ix - 2, iy), (ix - 10, iy), (0, 0, 255), 1)
        cv2.line(image, (ix, iy + 2), (ix, iy + 10), (0, 0, 255), 1)
        cv2.line(image, (ix, iy - 2), (ix, iy - 10), (0, 0, 255), 1)

        if x1[0] == 0:
            x1[0] = ix
            y1[0] = iy
            print('Double click a second x point')
        elif x1[0] != 0 and x1[1] == 0:
            x1[1] = ix
            y1[1] = iy
            prompt = '> '
            print("What is the x distance in meters between the 2 points?")
            deltax = float(input(prompt))
            dx = math.sqrt((x1[1] - x1[0]) ** 2 + (y1[1] - y1[0]) ** 2) * 0.05
            sx = deltax / dx
            print("You will need to choose the y coordinates vertically with respect to each other")
            print("Double click a y point")
        elif x1[1] != 0 and x1[2] == 0:
            x1[2] = ix
            y1

[2] = iy
            print("Double click a second y point")
        else:
            prompt = '> '
            print("What is the y distance in meters between the 2 points?")
            deltay = float(input(prompt))
            x1[3] = ix
            y1[3] = iy
            dy = math.sqrt((x1[3] - x1[2]) ** 2 + (y1[3] - y1[2]) ** 2) * 0.05
            sy = deltay / dy
            print(sx, sy)
            res = cv2.resize(image, None, fx=sx, fy=sy, interpolation=cv2.INTER_CUBIC)
            res = cv2.cvtColor(res, cv2.COLOR_BGR2GRAY)
            cv2.imwrite("KEC_BuildingCorrected.pgm", res)
            cv2.imshow("Image2", res)

            prompt = '> '
            print("What is the name of the new map?")
            mapName = input(prompt)

            prompt = '> '
            print("Where is the desired location of the maThis project is licensed under the [MIT LicenseLICENSE).](p and YAML file?")
            print("NOTE: if this program is not run on the TurtleBot, Please input the file location of where the map should be saved on TurtleBot. The file will be saved at that location on this computer. Please then transfer the files to TurtleBot.")
            mapLocation = input(prompt)
            completeFileNameMap = os.path.join(mapLocation, mapName + ".pgm")
            completeFileNameYaml = os.path.join(mapLocation, mapName + ".yaml")
            yaml = open(completeFileNameYaml, "w")
            cv2.imwrite(completeFileNameMap, res)

            yaml.write("image: " + mapLocation + "/" + mapName + ".pgm\n")
            yaml.write("resolution: 0.050000\n")
            yaml.write("origin: [" + str(-1) + "," + str(-1) + ", 0.000000]\n")
            yaml.write("negate: 0\noccupied_thresh: 0.65\nfree_thresh: 0.196")
            yaml.close()
            exit()

cv2.namedWindow('image', cv2.WINDOW_NORMAL)
cv2.setMouseCallback('image', draw_point)

while True:
    cv2.imshow('image', image)
    k = cv2.waitKey(20) & 0xFF
    if k == 27:
        break
    elif k == ord('a'):
        print('Done')

cv2.destroyAllWindows()
```

## Contact

If you have any questions or suggestions, feel free to contact me .